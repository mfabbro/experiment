# 
#  udpserver.rb
#  experiment
#  
#  Created by Mark Fabbro & Adair Lang on 2013-08-17.
#  Copyright 2013. All rights reserved.
# 


# TODO: Needs to ping to find if the neighburs exist or dont.


#
# This is a small test file that illustrates how to create cruddy UDP server.
#
require 'pp'
require 'socket'
require 'csv'
require './message_formats.rb'

include Socket::Constants

UDPFLAGS       = 0
HOST           = "10.0.1.2" #Marks HOST ID
PORT           = 4000
MAX_PACKET_LEN = 255

SYNC_INT   = 1000*10
$clkalg    = 'ats'
$ats_opts  = [0.3, 0.5, 0.5] # rho_eta, rho_alpha, rho_nu
$ftsp_opts = [2, 0.99, 1]    # root_timeout, delay_forget_fact, bool delay_enabled
$clc_opts  = [1, 1]          # delay_enabled_p, control_limiter_p
$kmr_opts  = [10, 0.9, 0.95] # root_timeout_p,  delay_forget_fact_p, skew_forget_fact_p


s = UDPSocket.new(Socket::AF_INET)
s.bind(HOST, PORT)

registered_clients = {}
addr_to_node_ids   = {}
node_ids_to_addr   = {}
next_nodeid        = 0

#
# Initial Registration of Nodes Part.
#
puts "\nPress R when registration has been completed..."
loop do
  # Check for UDP Packet.
  # Check for KB Press.
  data, sender = s.recvfrom_nonblock(MAX_PACKET_LEN) rescue nil

  # Hide Echos of Command Characters, and don't need return (raw)
  system("stty raw -echo")
  kb_char = STDIN.read_nonblock(1) rescue nil
  # Bring the echos back and turn off raw.
  system("stty -raw echo")             # bring echos back
  next if (data.nil? and kb_char.nil?) #nothing to do!
  if data
    remote_host = sender[3]
    remote_port = sender[1]
    node_addr   = [remote_host, remote_port]
    puts data

    # If we have received a registration initailisation packet.
    if RegistrationProtocol.reg_init_packet?(data)    
      # If received a RegInit from this address before, send back the already
      # allocated id. Otherwise, dish out a new node id!
      nodeid = (addr_to_node_ids[node_addr] ? addr_to_node_ids[node_addr] : (next_nodeid += 1))
      addr_to_node_ids[node_addr] = nodeid
      node_ids_to_addr[nodeid]    = node_addr
      
      #
      # Select Clock Synchroniation Algorithm.
      #
      case $clkalg
      when 'ats' 
        puts $clkalg
        s.send(RegistrationProtocol.ackn_packet(nodeid, 0, SYNC_INT, *$ats_opts),  UDPFLAGS, *node_addr)
      when 'ftsp' 
        puts $clkalg
        s.send(RegistrationProtocol.ackn_packet(nodeid, 1, SYNC_INT, *$ftsp_opts), UDPFLAGS, *node_addr)
      when 'clc'  
        puts $clkalg
        s.send(RegistrationProtocol.ackn_packet(nodeid, 2, SYNC_INT, *$clc_opts),  UDPFLAGS, *node_addr)
      when 'kmr'  
        puts $clkalg
        s.send(RegistrationProtocol.ackn_packet(nodeid, 3, SYNC_INT, *$kmr_opts),  UDPFLAGS, *node_addr)
      end
      puts "No. of Nodes Registered: #{addr_to_node_ids.length}"
    end
  end
  if (not kb_char.nil?) and  (kb_char.match /^R$/)
    puts "Registration Finalised."
    2.times do 
      node_ids_to_addr.each_pair do |id, addr|
        neighs = node_ids_to_addr.keys.reject{|x| x.eql? id}
        neighs = node_ids_to_addr.keys if neighs.empty?
        # neighs = node_ids_to_addr.keys
#        pp "Sending to #{addr}: #{neighs}"
        s.send(RegistrationProtocol.neiglst_packet(neighs), UDPFLAGS, *addr)
      end
    end
    break
  end
end

#
# Main Loop now that Registration is Complete.
#

@send_neighlist = false
@send_reset     = false
Thread.new {  
  while true 
    read = STDIN.read(1) rescue nil
    case read.chomp
    when 'R'
      @send_neighlist = true 
    when 'T'
      @send_reset     = true 
    end
  end
}

experiment_log  = "../logs/experiment_" + $test + ".csv"
experiment_dlog = "../logs/delay_" + $test + ".csv"
current_logid = 0
CSV.open(experiment_dlog, "wb") do |csv_dlog|
CSV.open(experiment_log, "wb") do |csv_log|    

time = Time.now
  while (current_logid < 300) do 
    before_recv = Time.now
    data, sender = s.recvfrom(MAX_PACKET_LEN)
    before_send = Time.now

    dest_addr = nil
    dest_addr = node_ids_to_addr[data.unpack("Z*C")[1]] if data.unpack("Z*C")[1]
    begin
      s.send(data, UDPFLAGS, *dest_addr)
      # try replying multiple times.
    rescue
      if logpack = RegistrationProtocol.reg_log_packet?(data)
        pp logpack
        csv_log << logpack
        current_logid = logpack[3]
      elsif delaypack = RegistrationProtocol.reg_dlog_packet?(data)
        csv_dlog << delaypack
      end
    end

    if @send_neighlist
      @send_neighlist = false
      node_ids_to_addr.each_pair do |id, addr|
        neighs = node_ids_to_addr.keys.reject{|x| x.eql? id}
        neighs = node_ids_to_addr.keys if neighs.empty?
        pp "Send Neigbour List"
        s.send(RegistrationProtocol.neiglst_packet(neighs), UDPFLAGS, *addr)
      end
    elsif @send_reset 
      @send_reset = false
      pp "Resetting Clock Sync."
      node_ids_to_addr.each_pair do |id, addr|
        s.send(RegistrationProtocol.reset_packet(), UDPFLAGS, *addr)      
      end
    end
  end
end
end
