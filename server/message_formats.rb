class RegistrationProtocol
  INIT_PROT_ID   = "reginit"
  RESET_PROT_ID  = "regrset"
  ACK_PROT_ID    = "regackn"
  NLST_PROT_ID   = "regneig"
  LOG_PROT_ID    = "reg:log"
  DLOG_PROT_ID   = "regdlog"
  MAX_NEIGHBOURS = 8

  def self.reg_init_packet?(mesg)
    return mesg.unpack("Z*")[0] == INIT_PROT_ID
  end
  
  def self.reg_log_packet?(mesg)
    vals = mesg.unpack("Z*CCNgn")
    if vals[0] != LOG_PROT_ID
      return false
    else
      return [vals[2], vals[3], vals[4], vals[5]]
    end
  end
  
  def self.reg_dlog_packet?(mesg)
    vals = mesg.unpack("Z*CCCNl>l>")
    if vals[0] != DLOG_PROT_ID
      return false
    else
      return [vals[2], vals[3], vals[4], vals[5], vals[6]]
    end
  end
  
  def self.reset_packet()
    return [RESET_PROT_ID].pack("Z*")
  end
  def self.ackn_packet(node_id, synch_protocol, synch_int, *options)
    # Always send six fields to have a consistent packet length.
    num_fields = 5

    # Z* is null terminated string, followed by unsigned 8 bitint32_t
    pack_str = ""
    options.each do |opt|
      case opt
      when Fixnum 
        pack_str << 'N'
      when Float
        pack_str << 'g'
      else raise "Don't know how to pack class type:#{opt.class}"
      end
    end
    # Pad up to number of fields length.
    (num_fields - pack_str.length).times do |x|
      pack_str << 'N'
      options << 0
    end
    return [ACK_PROT_ID, node_id, synch_protocol, synch_int, *options].pack("Z*CCN"+pack_str)
  end
  
  def self.neiglst_packet(neighbours)
    (MAX_NEIGHBOURS - neighbours.length).times do 
      neighbours << 0
    end
    # artificially add 1.
#    neighbours[0] = 1 # REMOVE THIS WHEN HAVE MORE THAN TWOOO!!!
    return [NLST_PROT_ID, *neighbours].pack("Z*C*")
  end
end
