%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 2013 - Capstone Project MC2 - Clock Synchronisation
%
% Created by Mark Fabbro and Adair Lang, September 2013.
% Copyright 2013 Mark Fabbro and Adair Lang. All rights reserved.
%
% Plot Experiment Results Function 
%
% Description: A script that produces plots from the log data files from
% an experiment.
%
% change the filename to csv file of log data from experiment
% The plot will be produced relative to node REFNODE.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


filename = 'experiment_final_kmr.csv';

raw_data = load(filename);
LOG      = 4;
NODEID   = 1;
VIRTCLK  = 2;
VIRTSKEW = 3;

REFNODE  = 2;

node_names  = unique(raw_data(:, NODEID))';
no_of_nodes = length(node_names);

hi_log   = max(raw_data(:, LOG))-1;

data_by_log_id = NaN*ones(hi_log, max(node_names));

for L=1:hi_log
    curr_log_data = raw_data(raw_data(:,LOG)==L, :);
    
    % Check if REFNODE has logged for this id.
    if curr_log_data(curr_log_data(:,NODEID) == REFNODE) 
        for k=node_names
            if ~isempty(curr_log_data(curr_log_data(:, NODEID)==k, VIRTCLK))
                data_by_log_id(L, k) = curr_log_data(curr_log_data(:, NODEID)==k, VIRTCLK);
            else
                data_by_log_id(L, k) = NaN;
            end
            if k ~= REFNODE
                data_by_log_id(L, k) = data_by_log_id(L, k) - curr_log_data(curr_log_data(:, NODEID)==REFNODE, VIRTCLK);
            end
        end        
    end
end

data_by_log_id = data_by_log_id(~isnan(data_by_log_id(:, REFNODE)), :);

%Figure 2 Virtual Clock error in steady state (after a start up
%period)
set(0,'DefaultAxesColorOrder',[1 0 0;0 1 0;0 0 1])
figure(4)
for node=node_names
    %This can be uncommented if the semilogy plot is changed to a normal
    %plot - it will add a dotted line along 0.
%     if node == REFNODE
%         h(node)=plot((data_by_log_id(:, REFNODE)-data_by_log_id(1, REFNODE))/(20000*60), zeros(1, length(data_by_log_id(:, REFNODE))), '--');
%         hold all
%     end
    if node ~= REFNODE 
        h(node)=plot((data_by_log_id(:, REFNODE)-data_by_log_id(1, REFNODE))/(20000*60), abs(data_by_log_id(:, node)/20));
                hold all
    end
end
title('Local Clock Error Relative to Node 1')
xlabel('Time (min)')
ylabel('Error (ms)')
hold off





