/*
 Loggger - Arduino
  At regular intervals sets a digital pin high to low to trigger a log from other guys
 */
int logger_pin = 3;
int led        = 13;

// the setup routine runs once when you press reset:
void setup() {                
  // initialize the digital pin as an output.
  pinMode(logger_pin, OUTPUT);   
  pinMode(led, OUTPUT);  
}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(logger_pin, !digitalRead(logger_pin));   // toggle LOG PIN
  digitalWrite(led, !digitalRead(led));   // toggle LED
  delay(500);               // wait for a second
}