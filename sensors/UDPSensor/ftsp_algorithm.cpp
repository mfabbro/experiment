#include "ftsp_algorithm.h"
#include "message_formats.h"
#include <TimerThree.h>
#include <arduino.h>
#include <stdint.h> 

volatile bool FTSPAlgorithm::synch_interval_expired = false;
volatile bool FTSPAlgorithm::keep_connection_alive = false;
volatile int32_t FTSPAlgorithm::synch_int   = 0;
volatile int32_t FTSPAlgorithm::sync_count = 0;
volatile int32_t FTSPAlgorithm::keep_count = KEEP_CONNECTION_ALIVE;
/* static */ void FTSPAlgorithm::timer_expired() {
  if (++sync_count >= synch_int) {
    synch_interval_expired = true;    
    sync_count = 0;
  }
  if (++keep_count >= KEEP_CONNECTION_ALIVE) {
    keep_connection_alive = true;
    keep_count = 0;
  }
}

volatile bool FTSPAlgorithm::perform_log = false;
volatile int32_t FTSPAlgorithm::log_time;
volatile float FTSPAlgorithm::log_skew;
FTSPAlgorithm *FTSPAlgorithm::self;
/* static */ void FTSPAlgorithm::log_stats() {
  perform_log = true;
  log_time    = self->virtual_clock(millis());
  log_skew    = self->skew;
}

void FTSPAlgorithm::initialize (uint8_t name_p, int32_t synch_int_p, uint8_t root_timeout_p, float forget_fact_p, bool delay_enabled_p) {
  self                  = this;
  name                  = name_p;
  my_id                 = name_p;
  synch_int             = synch_int_p;
  sync_count            = synch_int_p;
  root_timeout          = root_timeout_p;
  delay_enabled         = delay_enabled_p;
  root_change           = false;
  curr_root_est         = my_id;
  old_root              = 0;
  old_bc_no             = 0;
  broadcasts_since_root = 0;
  send_dummy_packet     = 0;

  ftsp_rx_packet.initialize();
  ftsp_tx_packet.initialize();
  reg_packet.initialize();

  skew = 0;
  offsetAverage  = 0;
  localAverage   = 0;

  buffer.clear();
  broadcast_no = 0;
  
  // SetUP Timer3 to set the 'do_routine_work' flag upon the 
  //  Timer3.initialize(synch_int * 1000); // Convert ms into microseconds.
  Timer3.initialize(1000L); // Convert ms into microseconds.
  Timer3.attachInterrupt(FTSPAlgorithm::timer_expired);
  Timer3.start();
  
  // Setup external interrupt to enable logging. replace with constants
  pinMode(2, INPUT);
  attachInterrupt(0, FTSPAlgorithm::log_stats, RISING);
}

void FTSPAlgorithm::routine_work() {
    if (perform_log) {
      // Serial.print("Interrupted:"); Serial.println(perform_log);
      perform_log = false;
      reg_packet.send_log_info(name, log_time, log_skew);    
    } 

    if (keep_connection_alive) {
      keep_connection_alive = false;
      reg_packet.send_dummy_packet();
    }
    
    if (synch_interval_expired) {
      Serial.println("Inside Synch Interval Expired");
      synch_interval_expired = false;
      
      //If root has timed out then set self to root.
      if((curr_root_est != my_id) && (broadcasts_since_root >= root_timeout)) {
        old_root      = curr_root_est;
        old_bc_no     = broadcast_no;
        curr_root_est = my_id;
        root_change   = true;   
      } //Else continue - root still exists.
    
      for (uint8_t i=0; i < MAX_NO_NEIGHBOURS; i++) {
        if (neighbours[i] != 0) {
          int32_t current_time = millis();
          ftsp_tx_packet.send_broadcast(neighbours[i], name, virtual_clock(current_time), broadcast_no, curr_root_est);
        }
      }

      if(curr_root_est == my_id) {
        broadcast_no++;
      } else {
        broadcasts_since_root ++;
      }
    }
}

void FTSPAlgorithm::recv_packet(udpdata *udp) {
  int32_t timestamp = udp->timestamp;
  if (delay_est.del_packet_arrived(udp)) {
    return; // Delay packet arrived, don't need to do any FTSP work yet.
  }

  if (reg_packet.reg_reset_arrived(udp)) {
    initialize(name, synch_int, root_timeout, 0, delay_enabled);
  }

  if (ftsp_rx_packet.broadcast_arrived(udp)) {    
    if (ftsp_rx_packet.bc_packet.root_est < curr_root_est) {
      if((ftsp_rx_packet.bc_packet.root_est == old_root) && (ftsp_rx_packet.bc_packet.bc_no == old_bc_no)) {
        return; //Other node has not timed out yet and is still brdcasting old info.
      }
      curr_root_est = ftsp_rx_packet.bc_packet.root_est;
      root_change   = true;
    } else if (curr_root_est == my_id) { 
      return; //If we are the root then don't need to update
    } else if ((ftsp_rx_packet.bc_packet.root_est > curr_root_est) || (ftsp_rx_packet.bc_packet.bc_no <= broadcast_no)) {
      Serial.print("Node "); Serial.print((int16_t) my_id); Serial.print(" received a redundant packet from "); 
      Serial.println((int16_t) ftsp_rx_packet.bc_packet.from); 
      return; // If their root est is worse than ours or if it is ours but with an old bc then ignore.
    }
     //If we reach here then we either have a packet from a new root or we have a new packet (bc_no) from the current root.
Serial.print("Node "); Serial.print((int16_t) my_id); Serial.print(" received a valid packet from "); 
Serial.println((int16_t) ftsp_rx_packet.bc_packet.from); 
    //Reset bc_since_root and update bc_number.
    broadcasts_since_root  = 0;
    broadcast_no           = ftsp_rx_packet.bc_packet.bc_no;   
    int32_t delay_roottime = (skew + 1.0) * delay_est.delay[ftsp_rx_packet.bc_packet.from];
    update_clock_params(ftsp_rx_packet.bc_packet.root_time + delay_roottime, timestamp);

    // Send a new delay packet off if delay estimator is on.
    if (delay_enabled) {
      delay_est.send_del_req(ftsp_rx_packet.bc_packet.from, name, millis());
    }
  }
}

void FTSPAlgorithm::update_clock_params(int32_t root_time, int32_t time_received) {
 
  if (root_change) {
    buffer.clear(); // REMOVE THIS IF WE WANT TO IN THE FUTURE, SINCE THEY SHOULD TRACK CLOSELY MAYBE.
    root_change = false;
  }
  Serial.println("I am about to add to Buffer");
  PP(time_received);
  int32_t time_diff =  root_time - time_received;
  PP(time_diff);
  PP(offsetAverage);
  PP(localAverage);
  buffer.add(time_received, root_time - time_received);
  do_linear_regression();
}

// Code modified form TimeSyncP.nc
void FTSPAlgorithm::do_linear_regression() {
    float newSkew = skew;
    int32_t  newLocalAverage;
    int32_t  newOffsetAverage;
    int32_t  localAverageRest;
    int32_t offsetAverageRest;

    // Volatile used since avr g++ optimisation bug has problems otherwise.
    // Rediculous.
    volatile int64_t localSum, offsetSum;

    int8_t i;

    for (i = 0; i < FTSP_BUFFER_SIZE && !buffer.entries[i].valid; ++i)
        ;

    if (i >= FTSP_BUFFER_SIZE ) // buffer is empty
        return;
/*
    We use a rough approximation first to avoid time overflow errors. The idea
    is that all times in the buffer should be relatively close to each other.
*/
    newLocalAverage  = buffer.entries[i].time_received;
    newOffsetAverage = buffer.entries[i].time_diff;

    localSum          = 0;
    localAverageRest  = 0;
    offsetSum         = 0;
    offsetAverageRest = 0;
    // Serial.println("Before Loop 1:");

    while( ++i < FTSP_BUFFER_SIZE ) {
        if( buffer.entries[i].valid) {
            localSum  += (int64_t)((buffer.entries[i].time_received - newLocalAverage) / buffer.valid_entries);
            offsetSum += (int64_t)((buffer.entries[i].time_diff - newOffsetAverage) / buffer.valid_entries);

            localAverageRest  += (int32_t)(buffer.entries[i].time_received - newLocalAverage) % buffer.valid_entries;
            offsetAverageRest += (int32_t)(buffer.entries[i].time_diff - newOffsetAverage) % buffer.valid_entries;
        }
    }
    newLocalAverage   = (int32_t) (localSum  + (localAverageRest  / buffer.valid_entries) + newLocalAverage);
    newOffsetAverage += (offsetSum + (offsetAverageRest / buffer.valid_entries));

    localSum = offsetSum = 0;
    for(i = 0; i < FTSP_BUFFER_SIZE; ++i)
        if( buffer.entries[i].valid == true ) {
          int64_t a = (int64_t) buffer.entries[i].time_received - (int64_t) newLocalAverage;
          int64_t b = (int64_t) buffer.entries[i].time_diff     - (int64_t) newOffsetAverage;
          localSum  += a * a;
          offsetSum += a * b;
        }

    if( localSum != 0 ) {
      newSkew = (float)offsetSum / (float)localSum;      
    }
    
    skew          = newSkew;
    offsetAverage = newOffsetAverage;
    localAverage  = newLocalAverage;
}

int32_t FTSPAlgorithm::virtual_clock(int32_t timestamp) {
  // REMEMBER THAT THIS FUNCTION IS CALLED INSIDE AN INTERRUPT.
  // DON'T USE SERIAL.
  int32_t error = timestamp - localAverage;
  error = (int32_t) (skew * error);
  return (error + timestamp + offsetAverage);
} 

// This code has been modelled off an implementation of FTSP on tinyOS
// found here: 
// http://tinyos.cvs.sourceforge.net/viewvc/tinyos/tinyos-2.x/tos/lib/ftsp/
void FTSPBuffer::add(int32_t time_received, int32_t time_diff) {  

  int16_t i, oldestItem = 0;
  int16_t  freeItem = -1;
  int32_t age, oldestTime = 0;
  
  if(valid_entries < FTSP_BUFFER_SIZE) {
    for (i = 0; i < FTSP_BUFFER_SIZE; i++) {
      if (!entries[i].valid) {
        entries[i].valid         = true;
        entries[i].time_received = time_received;
        entries[i].time_diff     = time_diff;
        valid_entries++;
        break;
      }
    } 
  } else { // Buffer full, find oldest to remove.

    valid_entries = 0;    

    for (i = 0; i < FTSP_BUFFER_SIZE; i++) {
      age = time_received - entries[i].time_received;

      // In order to handle incorrectly computed times for whatever reason.
      if (age >= 0x7FFFFFFFL) {
        entries[i].valid = false;
      }
      if (!entries[i].valid) {
        freeItem = i;
      } else {
        valid_entries++;
      }
      if (age >= oldestTime) {
        oldestTime = age;
        oldestItem = i;
      }      
    }
    
    if (freeItem < 0) {
      freeItem = oldestItem;
    } else {
      valid_entries++;
    }

    entries[freeItem].valid         = true;
    entries[freeItem].time_received = time_received;
    entries[freeItem].time_diff     = time_diff;
  }
}

void FTSPBuffer::clear() {
  int16_t i;
  valid_entries = 0;
  for (i = 0; i < FTSP_BUFFER_SIZE; i++)
    entries[i].valid = false;
}