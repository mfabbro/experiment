#ifndef MESSAGE_FORMATS_H_INCLUDED
#define MESSAGE_FORMATS_H_INCLUDED

#include <stdint.h>
#include <math.h>
#include "netcomms.h"

/* ------ MACRO Definitions --- */
#define MAX_NO_NEIGHBOURS 8
#define SERVER_ID         0
#define PROT_ID_LEN       8
#define DELAYPROT_FORGET_FACTOR 0.97
#define DELAY_INIT_COND 87 // This initial condition is derived from experiment.
#define NO_OF_UDP_RESENDS 4
#define KEEP_CONNECTION_ALIVE 72 // So Magic it HURTS!

#define ATS_PROTO_ID  0
#define FTSP_PROTO_ID 1
#define CLC_PROTO_ID  2
#define KMR_PROTO_ID  3

#define PROT_REG_INIT    "reginit"
#define PROT_REG_RESET   "regrset"
#define PROT_REG_ACKN    "regackn"
#define PROT_NEIGH_LIST  "regneig"
#define PROT_REG_LOG     "reg:log"
#define PROT_REG_DEL_LOG "regdlog"
#define PROT_DELAY_REQT  "dlyreqt"
#define PROT_DELAY_REPL  "dlyrepl"
#define PROT_ATS_BC      "atsbrdc"
#define PROT_FTSP_BC     "ftspbcp"
#define PROT_CLCA_BC     "clcabcp"
#define PROT_KMR_PW_INIT "kmrinit"
#define PROT_KMR_PW_RESP "kmrresp"

#define htons(x) ( ((x)<<8) | (((x)>>8)&0xFF) )
#define ntohs(x) htons(x)
#define htonl(x) ( ((x)<<24 & 0xFF000000UL) | \
                   ((x)<< 8 & 0x00FF0000UL) | \
                   ((x)>> 8 & 0x0000FF00UL) | \
                   ((x)>>24 & 0x000000FFUL) )
#define ntohl(x) htonl(x)
#define us2ms(x) ((x) / 1000)
#define PP(x) Serial.print(#x " : "); Serial.println(x)
#define PPF(x, y) Serial.print(#x " : "); Serial.println(x, y)
#define PP64(x) Serial.print(#x " : "); Serial.print((uint32_t)(x >> 32), HEX); Serial.println((uint32_t) x, HEX);
#define Print64(x, y) Serial.print(x " : "); Serial.print((uint32_t)(y >> 32), HEX); Serial.println((uint32_t) y, HEX);
#define ABS(x) ((x)<0 ? -(x) : (x))

/* -- Variable Declarations --- */
// An enumeration type for identifying protocols.
//enum synch_alg_t { ats = 0, carli = 1 , ftsp = 2, kumar = 3, invalid};
/* -- Registration Protocol --- */
typedef struct reg_init_packet { 
  char prot_id[PROT_ID_LEN];
} reg_init_packet;

typedef union generic_field {
  uint32_t ul;
  float f;
} generic_field;

typedef struct reg_ackn_packet { 
 char prot_id[PROT_ID_LEN];
 uint8_t name;
 uint8_t synch_protocol;
 int32_t synch_int;
 generic_field field1;
 generic_field field2;
 generic_field field3;
 generic_field field4;
 generic_field field5;
} reg_ackn_packet;

typedef struct reg_reest_packet { 
 char prot_id[PROT_ID_LEN];
} reg_reset_packet;

typedef struct reg_neighlst_packet { 
 char prot_id[PROT_ID_LEN];
 uint8_t neighbours[MAX_NO_NEIGHBOURS];
} reg_neighlst_packet;

typedef struct reg_log_packet { 
 char prot_id[PROT_ID_LEN];
 uint8_t  dest;
 uint8_t  my_id;
 int32_t virtual_time;
 float    skew_est;
 int16_t log_num;  
} reg_log_packet;

typedef struct reg_dlog_packet { 
 char prot_id[PROT_ID_LEN];
  uint8_t  dest;
  uint8_t  my_id;
  uint8_t  neigh;
  int32_t  timestamp;
  int32_t  inst_delay;
  int32_t  filt_delay;
} reg_dlog_packet;

class RegistrationProtocol {
public:
  reg_init_packet      init_packet;
  reg_reset_packet    reset_packet;
  reg_ackn_packet      ackn_packet;
  reg_neighlst_packet  neig_packet;
  reg_log_packet        log_packet;
  reg_dlog_packet      dlog_packet;
  int16_t                   log_id;

  void initialize();
  void send_reg_init();
  void send_dummy_packet();
  void send_log_info(uint8_t my_id, int32_t virtual_timestamp, float skew_est);
  void send_delay_log_info(uint8_t my_id, uint8_t neigh, int32_t timestamp, int32_t inst_delay, int32_t filt_delay);
  bool reg_ackn_arrived(udpdata *udp);
  bool reg_neighlst_arrived(udpdata *udp);
  bool reg_reset_arrived(udpdata *udp);
};
/* -- Delay Protocol --- */
typedef struct del_req_packet { 
 char prot_id[PROT_ID_LEN];
 uint8_t dest;
 uint8_t from;
 int32_t request_time;
} del_req_packet;

typedef struct del_rep_packet { 
 char prot_id[PROT_ID_LEN];
 uint8_t dest;
 uint8_t from;
 int32_t request_time;
} del_rep_packet;

class DelayEstimationProtocol {
public:
  static int32_t  delay[MAX_NO_NEIGHBOURS];
  static float delay_gain[MAX_NO_NEIGHBOURS];
  static float forget_fact;
  del_req_packet req_packet;;
  del_rep_packet rep_packet;;
  
  void send_del_req(uint8_t dest, uint8_t from, int32_t request_time);
  bool del_packet_arrived(udpdata *udp);
};

/* -- ATS Protocol --- */
typedef struct ats_bc_packet { 
  char prot_id[PROT_ID_LEN];
  uint8_t  dest;
  uint8_t  from;
  int16_t bc_no;
  int32_t tau;
  int32_t tau_ref;
  int32_t tau_hat_ref;
  float    alpha_hat;
} ats_bc_packet;

class ATSProtocol {
public:
  ats_bc_packet bc_packet;
  void send_broadcast(uint8_t dest, uint8_t from, int16_t bc_no, int32_t tau, int32_t tau_ref, 
    int32_t tau_hat_ref, float alpha_hat);
  bool broadcast_arrived(udpdata *udp);
  void initialize();
};

/* -- FTSP Protocol --- */
typedef struct ftsp_bc_packet { 
  char prot_id[PROT_ID_LEN];
  uint8_t  dest;
  uint8_t  from;
  int32_t  root_time;
  int16_t  bc_no;
  uint8_t  root_est;
} ftsp_bc_packet;

class FTSPProtocol {
public:
  ftsp_bc_packet bc_packet;
  void initialize();
  void send_broadcast(uint8_t dest, uint8_t from, int32_t root_time, 
                      int16_t bc_no, uint8_t root_est);
  bool broadcast_arrived(udpdata *udp);
};


/* -- CLCA Protocol --- */
typedef struct clca_bc_packet { 
  char prot_id[PROT_ID_LEN];
  uint8_t   dest;
  uint8_t   from;
  int32_t  from_time;
  int16_t  bc_no;
  uint8_t   no_of_neigh;
} clca_bc_packet;

class CLCAProtocol {
public:
  clca_bc_packet bc_packet;
  void send_broadcast(uint8_t dest, uint8_t from, int32_t from_time, 
    int16_t bc_no, uint8_t no_of_neigh);
  bool broadcast_arrived(udpdata *udp);
  void initialize();
};

/* -- Kumar Protocol --- */
typedef struct kmr_pw_init_packet { 
  char prot_id[PROT_ID_LEN];
  uint8_t   dest;
  uint8_t   from;
  int32_t   beta_hat;
  uint8_t   root_id;
  int32_t  root_bc_no;
  int32_t  pw_packet_no;
  int32_t   pw_offset;
  float     alpha_hat;
} kmr_pw_init_packet;

typedef struct kmr_pw_resp_packet { 
  char prot_id[PROT_ID_LEN];
  uint8_t   dest;
  uint8_t   from;
  int32_t  time_sent;
  int32_t  virt_time_received;
  int32_t  pw_packet_no;
} kmr_pw_resp_packet;

class KMRProtocol {
public:
  kmr_pw_init_packet pw_init_packet;
  kmr_pw_resp_packet pw_resp_packet;
  void send_pw_init(uint8_t dest, uint8_t from, int32_t beta_hat, uint8_t root_id, 
    int32_t root_bc_no, int32_t pw_offset, float alpha_hat, int32_t pw_packet_no);
  void send_pw_resp(uint8_t dest, uint8_t from, int32_t time_sent_us, int32_t virt_time_received_ms, int32_t pw_packet_no);
  void initialize();
  bool pw_init_arrived(udpdata *udp);
  bool pw_resp_arrived(udpdata *udp);
};
#endif
