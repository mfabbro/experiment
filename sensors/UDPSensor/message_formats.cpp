#include <stdint.h> 
#include "message_formats.h"
#include "stdio.h"
#include "string.h"
#include "netcomms.h"

/* Start Registration Protocol */
void RegistrationProtocol::initialize(){ 
  memcpy(init_packet.prot_id,  PROT_REG_INIT,    sizeof(PROT_REG_INIT));
  memcpy(ackn_packet.prot_id,  PROT_REG_ACKN,    sizeof(PROT_REG_ACKN));
  memcpy(neig_packet.prot_id,  PROT_NEIGH_LIST,  sizeof(PROT_NEIGH_LIST));
  memcpy(log_packet.prot_id,   PROT_REG_LOG,     sizeof(PROT_REG_LOG));
  memcpy(dlog_packet.prot_id,  PROT_REG_DEL_LOG, sizeof(PROT_REG_DEL_LOG));
  log_id = 0;
}

void RegistrationProtocol::send_reg_init() {
  g_udp_manager.send_packet((char *)&init_packet, sizeof(reg_init_packet));
}

void RegistrationProtocol::send_dummy_packet() {
  char fakepacket[] = "STAYALIVE";
  g_udp_manager.send_packet(fakepacket, sizeof(fakepacket));
}

bool RegistrationProtocol::reg_ackn_arrived(udpdata *udp) {
  if (udp->length == sizeof(reg_ackn_packet)) {
    reg_ackn_packet *data = (reg_ackn_packet *) &(udp->mesg);
    if (strncmp(data->prot_id, PROT_REG_ACKN, 
        sizeof(PROT_REG_ACKN)) == 0) {
      udp->length = 0;
      // Don't need to worry about endianness - 1 byte.
      ackn_packet.name           = data->name;
      ackn_packet.synch_protocol = data->synch_protocol;
      ackn_packet.synch_int      = ntohl(data->synch_int);
      ackn_packet.field1.ul      = ntohl(data->field1.ul);
      ackn_packet.field2.ul      = ntohl(data->field2.ul);
      ackn_packet.field3.ul      = ntohl(data->field3.ul);
      ackn_packet.field4.ul      = ntohl(data->field4.ul);
      ackn_packet.field5.ul      = ntohl(data->field5.ul);

      Serial.println("Received Reg Ackn");
      return true;
    }
  }
  return false;
}

bool RegistrationProtocol::reg_neighlst_arrived(udpdata *udp) {
  if (udp->length == sizeof(reg_neighlst_packet)) {
    reg_neighlst_packet *data = (reg_neighlst_packet *) &(udp->mesg);
    if (strncmp(data->prot_id, PROT_NEIGH_LIST, sizeof(PROT_NEIGH_LIST)) == 0) {
      // Don't need to worry about endianness - 1 byte.
      udp->length = 0;
      memcpy(neig_packet.neighbours , data->neighbours, sizeof(reg_neighlst_packet().neighbours));
      return true;
    }
  }
  return false;
}

bool RegistrationProtocol::reg_reset_arrived(udpdata *udp) {
  if (udp->length == sizeof(reg_reset_packet)) {
    reg_reset_packet *data = (reg_reset_packet *) &(udp->mesg);
    if (strncmp(data->prot_id, PROT_REG_RESET, sizeof(PROT_REG_RESET)) == 0) {
      // Don't need to worry about endianness - 1 byte.
      udp->length = 0;
      return true;
    }
  }
  return false;
}


void RegistrationProtocol::send_log_info(uint8_t my_id, int32_t virtual_timestamp, float skew_est) {
  log_packet.dest         = SERVER_ID;
  log_packet.my_id        = my_id;
  log_packet.virtual_time = htonl(virtual_timestamp);

  generic_field tmp;
  tmp.f               = skew_est;
  tmp.ul              = htonl(tmp.ul);

  log_packet.skew_est     = tmp.f;
  log_packet.log_num      = htons(log_id);
  log_id++;
  g_udp_manager.send_packet((char *)&log_packet, sizeof(reg_log_packet));
}

void RegistrationProtocol::send_delay_log_info(uint8_t my_id, uint8_t neigh, int32_t timestamp, int32_t inst_delay, int32_t filt_delay) {
  dlog_packet.dest       = SERVER_ID;
  dlog_packet.my_id      = my_id;
  dlog_packet.neigh      = neigh;
  dlog_packet.timestamp  = htonl(timestamp);
  dlog_packet.inst_delay = htonl(inst_delay);
  dlog_packet.filt_delay = htonl(filt_delay); 
  g_udp_manager.send_packet((char *)&dlog_packet, sizeof(reg_dlog_packet));
}
/*  END  Registration Protocol */

/* Start - Delay Estimation Protocol */
int32_t DelayEstimationProtocol::delay[] = {DELAY_INIT_COND};
float DelayEstimationProtocol::delay_gain[] = {0};
float DelayEstimationProtocol::forget_fact = DELAYPROT_FORGET_FACTOR;
void DelayEstimationProtocol::send_del_req(uint8_t dest, uint8_t from, int32_t request_time) {
    req_packet.dest         = dest;
    req_packet.from         = from;
    req_packet.request_time = htonl(request_time);
    g_udp_manager.send_packet((char *)&req_packet, sizeof(req_packet));
}


bool DelayEstimationProtocol::del_packet_arrived(udpdata *udp) {
  // Delay Request Arrived.
  if (udp->length == sizeof(del_req_packet)) {
    del_req_packet *data = (del_req_packet *) &(udp->mesg);
    if (strncmp(data->prot_id, req_packet.prot_id, sizeof(del_req_packet().prot_id)) == 0) {
      // Build Delay reply and send it off!
      udp->length = 0;
      rep_packet.dest         = data->from;
      rep_packet.from         = data->dest;
      rep_packet.request_time = data->request_time;
      g_udp_manager.send_packet((char *)&rep_packet, sizeof(rep_packet));
      return true;
    }
  } else if (udp->length == sizeof(del_rep_packet)) {
    del_rep_packet *data = (del_rep_packet *) &(udp->mesg);
    int32_t timestamp = udp->timestamp;
    if (strncmp(data->prot_id, rep_packet.prot_id, sizeof(del_rep_packet().prot_id)) == 0) {
      udp->length = 0;

      // Initialize delay_gain if it hasn't already.
      if (delay_gain[data->from] == 0) {delay_gain[data->from] = 1.0;}

      // update delay.
      delay[data->from] = delay[data->from] + 
        (int32_t) round(delay_gain[data->from] * ((0.5)*((int32_t) timestamp - (int32_t) ntohl(data->request_time)) - (int32_t)delay[data->from]));
      
      // update delay gain.
      delay_gain[data->from] = delay_gain[data->from] / (forget_fact + delay_gain[data->from]);

      // TEST
      RegistrationProtocol reg_packet;
      reg_packet.send_delay_log_info(data->dest, data->from, timestamp, (int32_t) timestamp - (int32_t) ntohl(data->request_time), delay[data->from]);
      return true;
    }
  }
  return false;
}
/* END - Delay Estimation Protocol */

/*  Start ATS Protocol */

void ATSProtocol::initialize(){ 
  memcpy(bc_packet.prot_id,  PROT_ATS_BC, sizeof(PROT_ATS_BC));
}

void ATSProtocol::send_broadcast(uint8_t dest, uint8_t from, int16_t bc_no, int32_t tau, 
 int32_t tau_ref, int32_t tau_hat_ref, float alpha_hat) {
    bc_packet.dest      = dest;
    bc_packet.from      = from;
    bc_packet.bc_no     = htons(bc_no);
    bc_packet.tau       = htonl(tau);
    bc_packet.tau_ref   = htonl(tau_ref);
    bc_packet.tau_hat_ref = htonl(tau_hat_ref);

    // Bugs will MOST likely be here.
    generic_field tmp;
    tmp.f               = alpha_hat;
    tmp.ul              = htonl(tmp.ul);
    // Bugs will MOST likely be here.
    
    bc_packet.alpha_hat = tmp.f;
    g_udp_manager.send_packet((char *)&bc_packet, sizeof(ats_bc_packet));
}

bool ATSProtocol::broadcast_arrived(udpdata *udp) {
  if (udp->length == sizeof(ats_bc_packet)) {
    ats_bc_packet *data = (ats_bc_packet *) &(udp->mesg);
    if (strncmp(data->prot_id, bc_packet.prot_id, sizeof(ats_bc_packet().prot_id)) == 0) {
      udp->length = 0;
      bc_packet.dest        = data->dest;
      bc_packet.from        = data->from;
      bc_packet.bc_no       = ntohs(data->bc_no);
      bc_packet.tau         = ntohl(data->tau);
      bc_packet.tau_ref     = ntohl(data->tau_ref);
      bc_packet.tau_hat_ref = ntohl(data->tau_hat_ref);
      // Bugs will MOST likely be here.
      generic_field tmp;
      tmp.f               = data->alpha_hat;
      tmp.ul              = htonl(tmp.ul);
      // Bugs will MOST likely be here.
      bc_packet.alpha_hat = tmp.f;
      return true;
    }
  }
  return false;
}
/*  END  ATS Protocol */

/*  Start FTSP Protocol */
void FTSPProtocol::initialize(){ 
  memcpy(bc_packet.prot_id,  PROT_FTSP_BC, sizeof(PROT_FTSP_BC));
}

void FTSPProtocol::send_broadcast(uint8_t dest, uint8_t from, int32_t root_time, 
  int16_t bc_no, uint8_t root_est) {
    bc_packet.dest      = dest;
    bc_packet.from      = from;
    bc_packet.root_time = htonl(root_time);
    bc_packet.bc_no     = htons(bc_no);
    bc_packet.root_est  = root_est;
    g_udp_manager.send_packet((char *)&bc_packet, sizeof(ftsp_bc_packet));
}

bool FTSPProtocol::broadcast_arrived(udpdata *udp) {
  if (udp->length == sizeof(ftsp_bc_packet)) {
    ftsp_bc_packet *data = (ftsp_bc_packet *) &(udp->mesg);
    if (strncmp(data->prot_id, PROT_FTSP_BC, sizeof(PROT_FTSP_BC)) == 0) {
      udp->length = 0;
      bc_packet.dest      = data->dest;
      bc_packet.from      = data->from;
      bc_packet.root_time = ntohl(data->root_time);
      bc_packet.bc_no     = ntohs(data->bc_no);
      bc_packet.root_est  = data->root_est;
      return true;
    }
  }
  return false;
}
/*  END  FTSP Protocol */

/*  Start CLCA Protocol */
void CLCAProtocol::initialize(){ 
  memcpy(bc_packet.prot_id,  PROT_CLCA_BC, sizeof(PROT_CLCA_BC));
}
void CLCAProtocol::send_broadcast(uint8_t dest, uint8_t from, int32_t from_time, 
  int16_t bc_no, uint8_t no_of_neigh) {
    bc_packet.dest       = dest;
    bc_packet.from       = from;
    bc_packet.from_time  = htonl(from_time);
    bc_packet.bc_no      = htons(bc_no);
    bc_packet.no_of_neigh= no_of_neigh;
    g_udp_manager.send_packet((char *)&bc_packet, sizeof(clca_bc_packet));
}

bool CLCAProtocol::broadcast_arrived(udpdata *udp) {
  if (udp->length == sizeof(clca_bc_packet)) {
    clca_bc_packet *data = (clca_bc_packet *) &(udp->mesg);
    if (strncmp(data->prot_id, bc_packet.prot_id, sizeof(clca_bc_packet().prot_id)) == 0) {
      udp->length = 0;
      bc_packet.dest         = data->dest;
      bc_packet.from         = data->from;
      bc_packet.from_time    = ntohl(data->from_time);
      bc_packet.bc_no        = ntohs(data->bc_no);
      bc_packet.no_of_neigh  = data->no_of_neigh;
      return true;
    }
  }
  return false;
}
/*  END  CLCA Protocol */

/*  Start Kumar Protocol */
void KMRProtocol::initialize(){ 
  memcpy(pw_init_packet.prot_id,  PROT_KMR_PW_INIT, sizeof(PROT_KMR_PW_INIT));
  memcpy(pw_resp_packet.prot_id,  PROT_KMR_PW_RESP, sizeof(PROT_KMR_PW_RESP));
}
void KMRProtocol::send_pw_init(uint8_t dest, uint8_t from, int32_t beta_hat,
  uint8_t root_id, int32_t root_bc_no, int32_t pw_offset, float alpha_hat, int32_t pw_packet_no) {
    pw_init_packet.dest          = dest;
    pw_init_packet.from          = from;
    pw_init_packet.beta_hat      = htonl(beta_hat);
    pw_init_packet.root_id       = root_id;
    pw_init_packet.root_bc_no    = htonl(root_bc_no);
    pw_init_packet.pw_offset     = htonl(pw_offset);
    // Bugs will MOST likely be here.
    generic_field tmp;
    tmp.f               = alpha_hat;
    tmp.ul              = htonl(tmp.ul);
    // Bugs will MOST likely be here.
    pw_init_packet.alpha_hat  = tmp.f; 
    pw_init_packet.pw_packet_no  = htonl(pw_packet_no);
    g_udp_manager.send_packet((char *)&pw_init_packet, sizeof(kmr_pw_init_packet));
}

void KMRProtocol::send_pw_resp(uint8_t dest, uint8_t from, int32_t time_sent_us, int32_t virt_time_received_ms, int32_t pw_packet_no) {
    pw_resp_packet.dest               = dest;
    pw_resp_packet.from               = from;
    pw_resp_packet.time_sent          = htonl(time_sent_us);
    pw_resp_packet.virt_time_received = htonl(virt_time_received_ms);
    pw_resp_packet.pw_packet_no       = htonl(pw_packet_no);
    g_udp_manager.send_packet((char *)&pw_resp_packet, sizeof(kmr_pw_resp_packet));
}

bool KMRProtocol::pw_init_arrived(udpdata *udp) {
  if (udp->length == sizeof(kmr_pw_init_packet)) {
    kmr_pw_init_packet *data = (kmr_pw_init_packet *) &(udp->mesg);
    if (strncmp(data->prot_id, pw_init_packet.prot_id, sizeof(kmr_pw_init_packet().prot_id)) == 0) {
      udp->length = 0;
      pw_init_packet.dest          = data->dest;
      pw_init_packet.from          = data->from;
      pw_init_packet.beta_hat      = ntohl(data->beta_hat);
      pw_init_packet.root_id       = data->root_id;
      pw_init_packet.root_bc_no    = ntohl(data->root_bc_no);
      pw_init_packet.pw_offset     = ntohl(data->pw_offset);
      pw_init_packet.pw_packet_no  = htonl(data->pw_packet_no);
      generic_field tmp;
      tmp.f  = data->alpha_hat;
      tmp.ul = ntohl(tmp.ul);

      pw_init_packet.alpha_hat = tmp.f; 
      return true;
    }
  }
  return false;
}

bool KMRProtocol::pw_resp_arrived(udpdata *udp) {
  if (udp->length == sizeof(kmr_pw_resp_packet)) {
    kmr_pw_resp_packet *data = (kmr_pw_resp_packet *) &(udp->mesg);
    if (strncmp(data->prot_id, pw_resp_packet.prot_id, sizeof(kmr_pw_resp_packet().prot_id)) == 0) {
      udp->length = 0;
      pw_resp_packet.dest               = data->dest;
      pw_resp_packet.from               = data->from;
      pw_resp_packet.time_sent          = ntohl(data->time_sent);
      pw_resp_packet.virt_time_received = ntohl(data->virt_time_received);
      pw_resp_packet.pw_packet_no       = ntohl(data->pw_packet_no);
      return true;
    }
  }
  return false;
}

/*  END  Kumar Protocol */



