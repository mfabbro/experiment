#include "message_formats.h"
#include "string.h"
#include "clock_algorithms.h"

uint8_t ClockAlgorithm::alg_type = 0xFF;
ClkAlgorithms ClockAlgorithm::alg;
void ClockAlgorithm::initialize(reg_ackn_packet *ackn_packet) {
  alg_type = ackn_packet->synch_protocol;
  switch (alg_type) {
    case ATS_PROTO_ID: {
    ATSAlgorithm new_alg;
    alg.ats = new_alg;
    //uint8_t name_p, int32_t synch_int_p, float rho_eta_p, float rho_v_p, float rho_o_p
    alg.ats.initialize(ackn_packet->name, ackn_packet->synch_int, ackn_packet->field1.f, 
      ackn_packet->field2.f, ackn_packet->field3.f); break;
    }
    case FTSP_PROTO_ID: {
    FTSPAlgorithm new_alg;
    alg.ftsp = new_alg;
    //uint8_t name_p, int32_t synch_int_p, uint8_t root_timeout_p, float forget_fact_p, bool delay_enabled_p
    alg.ftsp.initialize(ackn_packet->name, ackn_packet->synch_int, ackn_packet->field1.ul, 
        ackn_packet->field2.f, (bool) ackn_packet->field3.ul); break;
    }
    case CLC_PROTO_ID: {
    CLCAlgorithm new_alg;
    alg.clc = new_alg;
      //uint8_t name_p, int32_t synch_int_p, bool delay_enabled_p, bool control_limiter_p
    alg.clc.initialize( ackn_packet->name, ackn_packet->synch_int, (bool) ackn_packet->field1.ul,
        (bool) ackn_packet->field2.ul); break;
    }
    case KMR_PROTO_ID: {
    alg.kmr.initialize( ackn_packet->name, ackn_packet->synch_int,
                   (uint16_t) ackn_packet->field1.ul, ackn_packet->field2.f, 
                   ackn_packet->field3.f); break;
    }
   default: Serial.println("Unknown Clock Synchronisation Protocol ID.");
    }
}

void ClockAlgorithm::update_neighbours(uint8_t neighbours[]) {
  switch (alg_type) {
  case ATS_PROTO_ID:  memcpy(alg.ats.neighbours,  neighbours, MAX_NO_NEIGHBOURS); break;
  case FTSP_PROTO_ID: memcpy(alg.ftsp.neighbours, neighbours, MAX_NO_NEIGHBOURS); break;
  case CLC_PROTO_ID:  memcpy(alg.clc.neighbours,  neighbours, MAX_NO_NEIGHBOURS); break;
  case KMR_PROTO_ID:  memcpy(alg.kmr.neighbours,  neighbours, MAX_NO_NEIGHBOURS); break;
  }
}

void ClockAlgorithm::routine_work() {
  switch (alg_type) {
  case ATS_PROTO_ID:  alg.ats.routine_work();  break;
  case FTSP_PROTO_ID: alg.ftsp.routine_work(); break;
  case CLC_PROTO_ID:  alg.clc.routine_work();  break;
  case KMR_PROTO_ID:  alg.kmr.routine_work();  break;
  }
}

void ClockAlgorithm::recv_packet(udpdata *udp) {
  switch (alg_type) {
  case ATS_PROTO_ID:  alg.ats.recv_packet(udp);  break;
  case FTSP_PROTO_ID: alg.ftsp.recv_packet(udp); break;
  case CLC_PROTO_ID:  alg.clc.recv_packet(udp);  break;
  case KMR_PROTO_ID:  alg.kmr.recv_packet(udp);  break;
  }
}
int32_t ClockAlgorithm::virtual_clock(int32_t timestamp) {
  switch (alg_type) {
  case ATS_PROTO_ID:  alg.ats.virtual_clock(timestamp);  break;
  case FTSP_PROTO_ID: alg.ftsp.virtual_clock(timestamp); break;
  case CLC_PROTO_ID:  alg.clc.virtual_clock(timestamp);  break;
  case KMR_PROTO_ID:  alg.kmr.virtual_clock(timestamp);  break;
  }
}