#include "kumar_algorithm.h"
#include "message_formats.h"
#include <TimerThree.h>
#include <arduino.h>
#include <math.h>

volatile bool KMRAlgorithm::synch_interval_expired = false;
volatile bool KMRAlgorithm::keep_connection_alive = false;
volatile int32_t KMRAlgorithm::synch_int   = 0;
volatile int32_t KMRAlgorithm::sync_count = 0;
volatile int32_t KMRAlgorithm::keep_count =  KEEP_CONNECTION_ALIVE;
/* static */ void KMRAlgorithm::timer_expired() {
  if (++sync_count >= synch_int) {
    synch_interval_expired = true;    
    sync_count = 0;
  }
  if (++keep_count >= KEEP_CONNECTION_ALIVE) {
    keep_connection_alive = true;
    keep_count = 0;
  }
}

volatile bool KMRAlgorithm::perform_log = false;
volatile int32_t KMRAlgorithm::log_time;
volatile float KMRAlgorithm::log_skew;
KMRAlgorithm *KMRAlgorithm::self;
/* static */ void KMRAlgorithm::log_stats() {
  perform_log = true;
  log_time    = self->virtual_clock(millis());
  log_skew    = self->alpha_hat;
}

void KMRAlgorithm::initialize(uint8_t name_p, int32_t synch_int_p, int16_t root_timeout_p, float delay_forget_fact_p, float skew_forget_fact_p) {
  self               = this;
  name               = name_p;
  synch_int          = synch_int_p;
  sync_count        = synch_int_p;
  skew_forget_fact   = skew_forget_fact_p;
  delay_forget_fact  = delay_forget_fact_p;
  root_timeout       = root_timeout_p;
  current_root       = name;
  send_dummy_packet  = 0;
  bc_no              = 1;
  old_root           = name;
  old_root_bc_no     = 0;
  beta_hat           = 0;
  alpha_hat          = 1;
  bc_since_last_root = 0;
  pw_packet_no       = 0;

  kmr_rx_packet.initialize();
  kmr_tx_packet.initialize();
  reg_packet.initialize();

  // SetUP Timer3 to set the 'do_routine_work' flag upon the 
  //  Timer3.initialize(synch_int * 1000); // Convert ms into microseconds.
  Timer3.initialize(1000L); // Convert ms into microseconds.
  Timer3.attachInterrupt(KMRAlgorithm::timer_expired);
  Timer3.start();
  
  // Setup external interrupt to enable logging. replace with constants
  pinMode(2, INPUT);
  attachInterrupt(0, KMRAlgorithm::log_stats, RISING);
}

void KMRAlgorithm::routine_work() {
    if (perform_log) {
      perform_log = false;
      reg_packet.send_log_info(name, log_time, log_skew);    
    }
    
    if (keep_connection_alive) {
      keep_connection_alive = false;
      reg_packet.send_dummy_packet();
    }
    
    if (synch_interval_expired) {
      synch_interval_expired = false;

      if (bc_since_last_root > root_timeout) {
        old_root           = current_root;
        old_root_bc_no     = bc_no;
        current_root       = name;
        bc_since_last_root = 0;        
      }

      for (uint8_t i=0; i < MAX_NO_NEIGHBOURS; i++) {
        uint8_t n = neighbours[i];
          if (n != 0) {
            ++pw_packet_no;
            int32_t current_time = millis();
            kmr_tx_packet.send_pw_init(n, name, beta_hat, 
              current_root, bc_no, ndata[n].pw_offset, alpha_hat, pw_packet_no);
            ndata[n].last_me_sent = current_time;
          }          
      }
      if (current_root == name) 
        ++bc_no;
      else 
        ++bc_since_last_root;
    }
}

void KMRAlgorithm::recv_packet(udpdata *udp) {
  int32_t timestamp = udp->timestamp;
  // === Pair-Init Response Has Arrived.
  if (kmr_rx_packet.pw_init_arrived(udp)) {
    uint8_t n = kmr_rx_packet.pw_init_packet.from;  

    // Send Response
    int32_t time_sent_us = millis();
    kmr_tx_packet.send_pw_resp(n, name, time_sent_us, timestamp * alpha_hat, kmr_rx_packet.pw_init_packet.pw_packet_no);
    
    // Store Relavent Information
    ndata[n].n_beta_hat  = kmr_rx_packet.pw_init_packet.beta_hat;
    ndata[n].n_alpha_hat = kmr_rx_packet.pw_init_packet.alpha_hat;
    ndata[n].n_pw_offset = kmr_rx_packet.pw_init_packet.pw_offset;

    if (kmr_rx_packet.pw_init_packet.root_id < current_root) {
      if (kmr_rx_packet.pw_init_packet.root_id == old_root && 
        kmr_rx_packet.pw_init_packet.root_bc_no == old_root_bc_no) 
        return;
      current_root   = kmr_rx_packet.pw_init_packet.root_id;
      bc_no = kmr_rx_packet.pw_init_packet.root_bc_no;
      
    } else if (kmr_rx_packet.pw_init_packet.root_id == current_root &&
      bc_no < kmr_rx_packet.pw_init_packet.root_bc_no) {
        bc_since_last_root = 0;
        bc_no = kmr_rx_packet.pw_init_packet.root_bc_no;
    }
    ndata[n].pw_init_recvd = true;
  // === Pair-Wise Response Has Arrived.
  } else if (kmr_rx_packet.pw_resp_arrived(udp)) {
    // Serial.println("recv_packet -> pw_resp_arrived");
    update_pw(&kmr_rx_packet, timestamp);
  }
  
  if (current_root != name) {
    Serial.println("recv_packet -> Not Current_Root");
    do_spatial_smoothing();
  } else {
    Serial.println("I am Root: not doing spatial smoothing.");
  }
}

void KMRAlgorithm::update_pw(KMRProtocol *mesg, int32_t timestamp) { 
  kmr_neigh_data *nd = &ndata[mesg->pw_resp_packet.from];
  
  // Serial.println("\n\n\n\n\n************ Inside update_pw");

  // Base Assumption that no timestamp will ever be 0. 
  if (nd->n_last_sent == 0) {
    nd->initialize_rls = true;
    nd->pw_alpha       = 1.0;
    nd->delay          = 87;
    nd->delay_gain     = 1.0;
        
  } else {
    // This check makes sure that the message we are receiving is from our 
    // most recent init packet. Otherwise our delta's are bogus. In this case,
    // we only do work on the non delta based variables.
    if (mesg->pw_resp_packet.pw_packet_no == pw_packet_no) {
      nd->n_delta_sent     = mesg->pw_resp_packet.time_sent - nd->n_last_sent;
      nd->delta_me_recv    = timestamp - nd->last_me_recv;
    
      // If first pair of responses we need to initialize RLS to get faster convergence.
      if (nd->initialize_rls) {
        nd->initialize_rls = false;
        nd->pw_alpha       = (float) nd->n_delta_sent / (float) nd->delta_me_recv;
        nd->skew_rls_gain  = (1.0/nd->delta_me_recv) / nd->delta_me_recv;

      
      // If it isn't the first time, just perform standard RLS computations.
      } else {
        float tmp_rls_var = (nd->skew_rls_gain)*(nd->delta_me_recv);
        nd->skew_rls_gain = (nd->skew_rls_gain*skew_forget_fact) / (1.0 + tmp_rls_var*nd->delta_me_recv);
        nd->pw_alpha = nd->pw_alpha + nd->skew_rls_gain*nd->delta_me_recv*
                       (nd->n_delta_sent - nd->pw_alpha*nd->delta_me_recv);
      }
    }

    nd->delay      = (int32_t) (nd->delay + nd->delay_gain * (0.5*(timestamp - nd->last_me_sent) - (float) (nd->delay)));
    nd->delay_gain = nd->delay_gain  / (delay_forget_fact + nd->delay_gain);

    // NOTE THAT THIS ALPHA HAT IS 1/THE ALPHA HAT NOTATION WE ARE USED TO.
    nd->pw_offset     = mesg->pw_resp_packet.virt_time_received - 
      (int32_t) ((nd->last_me_sent + nd->delay) * alpha_hat);
    nd->pw_resp_recvd = true;
  }  

  nd->n_last_sent  = mesg->pw_resp_packet.time_sent;
  nd->last_me_recv = timestamp;
}

void KMRAlgorithm::do_spatial_smoothing() {
  bool ready = true;
  
  for(uint8_t i=0; i < MAX_NO_NEIGHBOURS; i++) {
    uint8_t n = neighbours[i];
    if (n != 0) {
      if (!ndata[n].pw_init_recvd || !ndata[n].pw_resp_recvd) {
        ready = false;
        break;
      }
    }
  }

  if (ready) {
    Serial.println("We are Ready!");
    // Sum pairwise offsets of neighs and reset acks.
    float alpha_tmp    = 0.0;
    int32_t offset_tmp = 0;
    uint8_t degree     = 0;
    
    for(uint8_t i=0; i < MAX_NO_NEIGHBOURS; i++) {
      uint8_t n = neighbours[i];
      if (n != 0) {
        alpha_tmp  += log(ndata[n].n_alpha_hat) + log(ndata[n].pw_alpha);
        offset_tmp += -ndata[n].n_beta_hat + ndata[n].n_pw_offset;
        ndata[n].pw_init_recvd = false;
        ndata[n].pw_resp_recvd = false;
        degree += 1;
      }
    }
    // If there are no neighbours then declare self as root and stop updating.
    if (degree == 0) {
      // Serial.println("Degree == 0");
      old_root           = current_root;
      old_root_bc_no     = bc_no;
      current_root       = name;
      bc_since_last_root = 0;
      return;
    }
    // Update Clock Parameters.
    alpha_hat = exp(alpha_tmp/degree);
    beta_hat  = (-offset_tmp)/degree;
  }
}

int32_t KMRAlgorithm::virtual_clock(int32_t timestamp) {
  return (int32_t) ((timestamp * alpha_hat) + beta_hat);
} 
