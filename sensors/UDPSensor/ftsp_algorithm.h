#ifndef FTSPALGORITHM_H_INCLUDED
#define FTSPALGORITHM_H_INCLUDED
#include <stdint.h>
#include "message_formats.h"

#define FTSP_BUFFER_SIZE 4
// Time synch inteveral 

typedef struct ftsp_buffer_element {
  bool  valid;
  int32_t  time_diff;
  int32_t time_received;
} ftsp_buffer_element;

class FTSPBuffer {
public:
  int16_t valid_entries;
  ftsp_buffer_element entries[FTSP_BUFFER_SIZE];  
  
  void add(int32_t time_received, int32_t time_diff);
  void clear();
};

class FTSPAlgorithm {
  // -- Instance Variables
  int16_t   old_bc_no, broadcast_no;
  uint8_t    my_id, curr_root_est, root_timeout, old_root, broadcasts_since_root;
  bool       root_change, delay_enabled;
  float      skew;
  int32_t    offsetAverage;
  int32_t   localAverage;
  int16_t   send_dummy_packet;
  
  static FTSPAlgorithm *self;
  static volatile int32_t synch_int;
  static volatile bool synch_interval_expired;
  static volatile bool  keep_connection_alive;
  static volatile bool  perform_log;
  static volatile float    log_skew;
  static volatile int32_t log_time;
  static volatile int32_t keep_count;
  static volatile int32_t sync_count;

  FTSPBuffer buffer;
  FTSPProtocol ftsp_rx_packet;
  FTSPProtocol ftsp_tx_packet;
  RegistrationProtocol reg_packet;
  DelayEstimationProtocol delay_est;

public:
  uint8_t name;
  uint8_t neighbours[MAX_NO_NEIGHBOURS]; // Current Neighbours.

  // method declarations (prototypes)
  void routine_work();
  void initialize(uint8_t name_p, int32_t synch_int_p, uint8_t root_timeout_p, float forget_fact_p, bool delay_enabled_p);
  void recv_packet(udpdata *udp);
  void do_linear_regression();
  // void do_linear_regression_test();
  void update_clock_params(int32_t root_time, int32_t time_received);
  int32_t virtual_clock(int32_t timestamp); 
  static void timer_expired();
  static void log_stats();
};
#endif
