#include "carli_algorithm.h"
#include "message_formats.h"
#include <TimerThree.h>
#include <arduino.h>

volatile bool CLCAlgorithm::synch_interval_expired = false;
volatile bool CLCAlgorithm::keep_connection_alive = false;
volatile int32_t CLCAlgorithm::synch_int   = 0;
volatile int32_t CLCAlgorithm::sync_count = 0;
volatile int32_t CLCAlgorithm::keep_count =  KEEP_CONNECTION_ALIVE;
/* static */ void CLCAlgorithm::timer_expired() {
  if (++sync_count >= synch_int) {
    synch_interval_expired = true;    
    sync_count = 0;
  }
  if (++keep_count >= KEEP_CONNECTION_ALIVE) {
    keep_connection_alive = true;
    keep_count = 0;
  }
}

volatile bool CLCAlgorithm::perform_log = false;
volatile int32_t CLCAlgorithm::log_time;
volatile float CLCAlgorithm::log_skew;
CLCAlgorithm *CLCAlgorithm::self;
/* static */ void CLCAlgorithm::log_stats() {
  perform_log = true;
  log_time    = self->virtual_clock(millis());
  log_skew    = self->alpha_hat;
}

void CLCAlgorithm::initialize(uint8_t name_p, int32_t synch_int_p, bool delay_enabled_p, bool control_limiter_p) {
  self                  = this;
  name                  = name_p;
  synch_int             = synch_int_p;
  sync_count            = synch_int_p;
  delay_enabled         = delay_enabled_p;
  control_limiter_on    = control_limiter_p;
  invalid_control_limit = (synch_int*20);//*1000 when using micros. 
  control_gain          = 1.0/(synch_int*20); //*1000 when using micros. //* 20 in our new tic rate.
  alpha_hat             = 1.0;
  beta_hat              = 0;
  broadcast_no          = 0;
  init_period           = 2;


  clca_rx_packet.initialize();
  clca_tx_packet.initialize();
  reg_packet.initialize();

  // Initialize the clca_neigh_buffer to say that no data has arrived yet.
  for (uint8_t i=0; i < MAX_NO_NEIGHBOURS; i++) {
    clca_neigh_buffer[i].updated = false;
  } 

  // SetUP Timer3 to set the 'do_routine_work' flag upon the 
  //  Timer3.initialize(synch_int * 1000); // Convert ms into microseconds.
  Timer3.initialize(1000L); // Convert ms into microseconds.
  Timer3.attachInterrupt(CLCAlgorithm::timer_expired);
  Timer3.start();
  
  // Setup external interrupt to enable logging. replace with constants
  pinMode(2, INPUT);
  attachInterrupt(0, CLCAlgorithm::log_stats, RISING);
}

int8_t CLCAlgorithm::neighbours_count() {
  uint8_t count =0;
  for (uint8_t i=0; i < MAX_NO_NEIGHBOURS; i++) {
    if (neighbours[i] != 0)
      ++count;
  }
  return count;
}

void CLCAlgorithm::routine_work() {
    if (perform_log) {
      perform_log = false;
      reg_packet.send_log_info(name, log_time, log_skew);    
    }
    
    if (keep_connection_alive) {
      keep_connection_alive = false;
      reg_packet.send_dummy_packet();
    }
    
    if (synch_interval_expired) {
      synch_interval_expired = false;
 
      for (uint8_t i=0; i < MAX_NO_NEIGHBOURS; i++) {
          if (neighbours[i] != 0) {
            int32_t current_time = millis();
            clca_tx_packet.send_broadcast(neighbours[i], name, virtual_clock(current_time), broadcast_no, neighbours_count());
          }
      }
      broadcast_no++;
    }
}

void CLCAlgorithm::recv_packet(udpdata *udp) {
  int32_t timestamp = udp->timestamp;
  if (delay_est.del_packet_arrived(udp)) {
    return; // Delay packet arrived, don't need to do any FTSP work yet.
  }
  
  if (clca_rx_packet.broadcast_arrived(udp)) {  
    uint8_t neigh = clca_rx_packet.bc_packet.from;  
    clca_neigh_buffer[neigh].bc_no     = clca_rx_packet.bc_packet.bc_no;
    clca_neigh_buffer[neigh].no_neighs = clca_rx_packet.bc_packet.no_of_neigh;
    clca_neigh_buffer[neigh].time_diff = (int32_t)((int32_t) clca_rx_packet.bc_packet.from_time - (int32_t) virtual_clock(timestamp)) + (int32_t)(alpha_hat * delay_est.delay[neigh]);       
    clca_neigh_buffer[neigh].updated   = true;
    clca_neigh_buffer[neigh].kappa     = (clca_neigh_buffer[neigh].no_neighs > neighbours_count()) ? 1.0/(clca_neigh_buffer[neigh].no_neighs) : (1.0/(neighbours_count()));    

    if (send_dummy_packet++ == 300) {
      send_dummy_packet = 0;
      reg_packet.send_dummy_packet();
    }

    if(name == 0) {
    } else {
     update_clock_params();  
    }
    // Send a new delay packet off if delay estimator is on.
    if (delay_enabled) {
      // Note that the delays use micros to get higher resolution.!
      delay_est.send_del_req(clca_rx_packet.bc_packet.from, name, millis());
    }
  }
}

void CLCAlgorithm::update_clock_params() {
  for (uint8_t i=0; i < MAX_NO_NEIGHBOURS; i++) {
    if (neighbours[i] != 0) {
      if (!clca_neigh_buffer[neighbours[i]].updated) {
        return;
      }
    }
  } //If we reach here then we have new info from every neighbour
  int32_t u_i = 0;
  for (uint8_t i=0; i < MAX_NO_NEIGHBOURS; i++) {
    if (neighbours[i] != 0) {
      u_i += (int32_t)(clca_neigh_buffer[neighbours[i]].kappa * (float) clca_neigh_buffer[neighbours[i]].time_diff);
      clca_neigh_buffer[neighbours[i]].updated = false; // Reset to false 
    }
  }
  u_i = u_i / 2;
  beta_hat  +=  u_i; 
  if (init_period > 0) {
    init_period--;
  } else {
    alpha_hat += control_gain * (float) u_i;
    beta_hat  -= (int32_t)( control_gain * (float) u_i * (float) millis());  
  }
}

int32_t CLCAlgorithm::virtual_clock(int32_t timestamp) {
    return (int32_t) ((alpha_hat * timestamp) + beta_hat);
}