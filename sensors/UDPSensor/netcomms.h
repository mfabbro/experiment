#ifndef NETCOMMS_H_INCLUDED
#define NETCOMMS_H_INCLUDED
#include <Wifi.h>
#include <WiFiUdp.h>
#include <stdint.h> 

/* ------ MACRO Definitions --- */
#define UDP_BUFFER_SIZE  16
#define MAX_DGRAM_LEN    80  // Received UDP datagram buffer length
#define SERIAL_BAUD_RATE 115200

typedef struct udpdata {
  uint8_t mesg[MAX_DGRAM_LEN];
  bool    valid;
  int32_t timestamp;
  int8_t  length;
} udpdata;

/* ----- Class Declarations --- */
class UDPManager {
  static WiFiUDP          udp_sock;
  static IPAddress       server_ip;
  static uint16_t      server_port;
  static uint16_t   udplisten_port;
public:
  static udpdata udpbuffer[UDP_BUFFER_SIZE];
  
  void initialize_udp();
  void grab_udp_packets();
  // int32_t unread_datagram_exists();
  // void grab_packet();
  void send_packet(char packet[], int16_t len);
};
extern UDPManager g_udp_manager;    // Global access to Udp port that represents network.

/* -- Function Declarations --- */
extern void setup_wifi();       // Connects Arduino to the Wifi Network.
extern void printWifiStatus();  // Prints through serial Details about network.

#endif
