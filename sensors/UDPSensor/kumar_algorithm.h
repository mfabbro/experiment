#ifndef KMRALGORITHM_H_INCLUDED
#define KMRALGORITHM_H_INCLUDED
#include <stdint.h>
#include "message_formats.h"

typedef struct kmr_neigh_data {
  bool initialize_rls, pw_init_recvd, pw_resp_recvd;
  float pw_alpha, n_alpha_hat, skew_rls_gain, delay_gain;
  int32_t pw_offset, n_pw_offset, n_beta_hat;
  int32_t delay, n_delta_sent, delta_me_recv, n_last_sent, last_me_sent, last_me_recv;
} kmr_neigh_data;

class KMRAlgorithm {
  // -- Instance Variables
  // int16_t   synch_int, broadcast_no;
  // float delay_forget_factor, skew_forget_fact;
  // bool alpha_steady;
  // int8_t current_root;
  // int16_t current_root_bc_no;

  static KMRAlgorithm *self;
  static volatile bool  synch_interval_expired;
  static volatile bool  perform_log;
  static volatile bool  keep_connection_alive;
  static volatile float    log_skew;
  static volatile int32_t log_time;
  static volatile int32_t keep_count;
  static volatile int32_t sync_count;
  static volatile int32_t synch_int;

  kmr_neigh_data ndata[MAX_NO_NEIGHBOURS];
  float skew_forget_fact;
  float delay_forget_fact;
  float alpha_hat;
  int16_t     send_dummy_packet;
  int32_t beta_hat;
  int16_t root_timeout;
  int16_t bc_since_last_root;
  int8_t old_root;
  int8_t current_root;
  int16_t bc_no;
  int16_t old_root_bc_no;
  int32_t pw_packet_no;
  
  KMRProtocol kmr_tx_packet;
  KMRProtocol kmr_rx_packet;
  RegistrationProtocol reg_packet;
  
public:
  uint8_t name;
  uint8_t neighbours[MAX_NO_NEIGHBOURS]; // Current Neighbours.

  // method declarations (prototypes)
  void routine_work();
  void initialize(uint8_t name_p, int32_t synch_int_p, int16_t root_timeout_p, float delay_forget_fact_p, float skew_forget_fact_p);
  void recv_packet(udpdata *udp);
  void update_pw(KMRProtocol *mesg, int32_t timestamp);
  void do_spatial_smoothing();
  int32_t virtual_clock(int32_t timestamp); 
  static void timer_expired();
  static void log_stats();
};
#endif
