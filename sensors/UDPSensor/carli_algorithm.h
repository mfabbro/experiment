#ifndef CLCALGORITHM_H_INCLUDED
#define CLCALGORITHM_H_INCLUDED
#include <stdint.h>
#include "message_formats.h"

#define CONTROL_LIMITER_VAL 0.005

typedef struct carli_neigh_data {
   uint8_t no_neighs;
   bool updated;
   int16_t  bc_no;
   int32_t time_diff;
   float kappa;
  } carli_neigh_data;

class CLCAlgorithm {
  // -- Instance Variables
  int16_t   broadcast_no;
  bool       control_limiter_on, delay_enabled;
  int32_t    invalid_control_limit;
  int32_t   init_period;
  int16_t     send_dummy_packet; 
  static CLCAlgorithm *self;
  static volatile int32_t keep_count;
  static volatile int32_t sync_count;
  static volatile bool  keep_connection_alive;
  static volatile int32_t synch_int;
  static volatile bool  synch_interval_expired;
  static volatile bool  perform_log;
  static volatile float    log_skew;
  static volatile int32_t log_time;

  carli_neigh_data  clca_neigh_buffer[MAX_NO_NEIGHBOURS];
  int32_t  beta_hat;
  float alpha_hat, control_gain;
  DelayEstimationProtocol delay_est;
  RegistrationProtocol reg_packet;
  CLCAProtocol clca_tx_packet;
  CLCAProtocol clca_rx_packet;

public:
  uint8_t name;
  uint8_t neighbours[MAX_NO_NEIGHBOURS]; // Current Neighbours.

  // method declarations (prototypes)
  void routine_work();
  void initialize(uint8_t name_p, int32_t synch_int_p, bool delay_enabled_p, bool control_limiter_p);
  void recv_packet(udpdata *udp);
  void update_clock_params();
  int32_t virtual_clock(int32_t timestamp); 
  static void timer_expired();
  static void log_stats();
  int8_t neighbours_count();
};
#endif
