#include "message_formats.h"
#include "netcomms.h"
#include "ats_algorithm.h"
#include "ftsp_algorithm.h"
// #include "ftsp_lite.h"
#include "carli_algorithm.h"
#include "kumar_algorithm.h"

typedef union ClkAlgorithms {
  ATSAlgorithm   ats;
  CLCAlgorithm   clc;
  FTSPAlgorithm ftsp;
  KMRAlgorithm   kmr;
} ClkAlgorithms;

class ClockAlgorithm {
  static ClkAlgorithms alg;
  static uint8_t alg_type;
public:
  void initialize(reg_ackn_packet *ackn_packet);
  void routine_work();
  void recv_packet(udpdata *udp);
  void update_neighbours(uint8_t neighbours[]);
  int32_t virtual_clock(int32_t timestamp);
};