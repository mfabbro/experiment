#ifndef ATSALGORITHM_H_INCLUDED
#define ATSALGORITHM_H_INCLUDED
#include <stdint.h> 
#include "message_formats.h"


typedef struct ats_data {
  int32_t  tau_j;
  int32_t  tau_i;
  float    eta_ij;
  int16_t  bc_no;
} ats_data;

class ATSAlgorithm {
  // -- Instance Variables
  float       rho_eta, rho_v, rho_o;
  float       alpha_hat;
  int16_t     send_dummy_packet;
  int16_t     bc_no;
  int32_t     omicron_hat;
  int32_t     tau_i_ref;
  int32_t     tau_hat_i_ref;
  int8_t      resend_counter;
  
  static ATSAlgorithm *self;
  static volatile bool  synch_interval_expired;
  static volatile bool  keep_connection_alive;
  static volatile bool  perform_log;
  static volatile float    log_skew;
  static volatile int32_t log_time;
  static volatile int32_t keep_count;
  static volatile int32_t sync_count;
  static volatile int32_t synch_int;
  
  ats_data     last_synch_data[MAX_NO_NEIGHBOURS];

  DelayEstimationProtocol delay_est;
  RegistrationProtocol   reg_packet;
  ATSProtocol ats_rx_packet;
  ATSProtocol ats_tx_packet;

public:
  uint8_t name;
  uint8_t neighbours[MAX_NO_NEIGHBOURS];

  // method declarations (prototypes)
  void routine_work();
  void initialize(uint8_t name_p, int32_t synch_int_p, float rho_eta_p, float rho_v_p, float rho_o_p);
  void broadcast_to_neighbours();
  void recv_packet(udpdata *udp);
  int32_t virtual_clock(int32_t timestamp); 
  static void log_stats();
  static void timer_expired();
};
#endif