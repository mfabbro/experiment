#include <stdint.h> 
#include "ats_algorithm.h"
#include "message_formats.h"
#include <TimerThree.h>
#include <arduino.h>


volatile bool ATSAlgorithm::synch_interval_expired = false;
volatile bool ATSAlgorithm::keep_connection_alive = false;
volatile int32_t ATSAlgorithm::synch_int   = 0;
volatile int32_t ATSAlgorithm::sync_count = 0;
volatile int32_t ATSAlgorithm::keep_count = KEEP_CONNECTION_ALIVE;
/* static */ void ATSAlgorithm::timer_expired() {
  if (++sync_count >= synch_int) {
    synch_interval_expired = true;    
    sync_count = 0;
  }
  if (++keep_count >= KEEP_CONNECTION_ALIVE) {
    keep_connection_alive = true;
    keep_count = 0;
  }
}

volatile int32_t ATSAlgorithm::log_time;
volatile float ATSAlgorithm::log_skew;
volatile bool ATSAlgorithm::perform_log = false;
ATSAlgorithm *ATSAlgorithm::self;
/* static */ void ATSAlgorithm::log_stats() {
  perform_log = true;
  log_time    = self->virtual_clock(millis());
  log_skew    = self->alpha_hat;
}

void ATSAlgorithm::initialize (uint8_t name_p, int32_t synch_int_p, float rho_eta_p, float rho_v_p, float rho_o_p) {
  self           = this;
  name           = name_p;
  synch_int      = synch_int_p;
  sync_count     = synch_int_p / 2;
  resend_counter = 0;
  send_dummy_packet = 0;
  rho_eta        = rho_eta_p;
  rho_v          = rho_v_p;
  rho_o          = rho_o_p;
  bc_no          = 0;
  alpha_hat      = 1.0;
  tau_i_ref      = millis();
  tau_hat_i_ref  = tau_i_ref;
  perform_log    = false;
  
  ats_rx_packet.initialize();
  ats_tx_packet.initialize();
  reg_packet.initialize();
  
  for(uint8_t i=0; i < MAX_NO_NEIGHBOURS; i++) {
    last_synch_data[i].eta_ij = 0.0;
    last_synch_data[i].tau_j  = 0;
    last_synch_data[i].tau_i  = 0;
    last_synch_data[i].bc_no  = 0;
  }
  
  // SetUP Timer3 to set the 'do_routine_work' flag upon the 
  //  Timer3.initialize(synch_int * 10000); // Convert ms into microseconds.
  Timer3.initialize(1000L); // Convert ms into microseconds.
  Timer3.attachInterrupt(ATSAlgorithm::timer_expired);
  Timer3.start();
  
  // Setup external interrupt to enable logging. replace with constants
  pinMode(2, INPUT);
  attachInterrupt(0, ATSAlgorithm::log_stats, RISING);
}

void ATSAlgorithm::routine_work() {
    if (perform_log) {
      Serial.print("Interrupted:"); Serial.println(perform_log);
      perform_log = false;
      reg_packet.send_log_info(name, log_time, log_skew);    
    } 
    
    if (keep_connection_alive) {
      keep_connection_alive = false;
      reg_packet.send_dummy_packet();
    }
    
    if (synch_interval_expired) {
      synch_interval_expired = false;
      ++bc_no;
      broadcast_to_neighbours();
      resend_counter = NO_OF_UDP_RESENDS;
    }
}

void ATSAlgorithm::broadcast_to_neighbours() {
  for (uint8_t i = 0; i < MAX_NO_NEIGHBOURS; i++) {
    if (neighbours[i] != 0) {
      int32_t current_time = millis();
      ats_tx_packet.send_broadcast(neighbours[i], name, bc_no, current_time, tau_i_ref, tau_hat_i_ref, alpha_hat);
    }
  }  
}

void ATSAlgorithm::recv_packet(udpdata *udp) {
  int32_t timestamp = udp->timestamp;
  if (delay_est.del_packet_arrived(udp)) {
    return; // Delay packet arrived, don't need to do any ATS work yet.
  }
  
  if (reg_packet.reg_reset_arrived(udp)) {
    initialize(name, synch_int, rho_eta, rho_v, rho_o);
  }
  
  if (ats_rx_packet.broadcast_arrived(udp)) {    
    uint8_t   name_j  = ats_rx_packet.bc_packet.from;
    int16_t  rx_bc_no = ats_rx_packet.bc_packet.bc_no;
  
    if (rx_bc_no > last_synch_data[name_j].bc_no) {
      last_synch_data[name_j].bc_no = rx_bc_no;
  
      int32_t delta_tau_j, delta_tau_i;
      float eta_ij;
      int32_t tau_j         = ats_rx_packet.bc_packet.tau;
      int32_t tau_j_ref     = ats_rx_packet.bc_packet.tau_ref;
      int32_t tau_hat_j_ref = ats_rx_packet.bc_packet.tau_hat_ref;
      float   alpha_hat_j   = ats_rx_packet.bc_packet.alpha_hat;
      // Arbitrarily chosen to be volatile so the tau_hat_i_ref line compliles below
      // this is due to the compiler trying to do smart tricks to make the expression
      // easier to evaluate or something. But it fails cause it's an idiot.
      
      volatile int32_t tau_i         = timestamp;
  
      if (last_synch_data[name_j].eta_ij == 0.0) {
        last_synch_data[name_j].eta_ij = 1.0;
        last_synch_data[name_j].tau_j  = tau_j + delay_est.delay[name_j];
        last_synch_data[name_j].tau_i  = tau_i;
      } else {
      // Adjust tau_j to be our estimate of their time when we received the packet.
      tau_j +=  (int32_t)(last_synch_data[name_j].eta_ij * delay_est.delay[name_j]);
      
        // int32_t omicron_i;
        delta_tau_j = tau_j - last_synch_data[name_j].tau_j;
        delta_tau_i = tau_i - last_synch_data[name_j].tau_i;    
  
        tau_hat_i_ref = rho_o * (alpha_hat * (tau_i - tau_i_ref) + tau_hat_i_ref) +
            (1.0 - rho_o) * (alpha_hat_j * (tau_j - tau_j_ref) + tau_hat_j_ref);
            
        eta_ij = (rho_eta * last_synch_data[name_j].eta_ij) + 
          ((1.0 - rho_eta) * ((float) delta_tau_j / (float) delta_tau_i));
  
        alpha_hat = (rho_v * alpha_hat) + ((1.0 - rho_v) * (eta_ij * alpha_hat_j));
  
        tau_i_ref = tau_i;
  
        last_synch_data[name_j].eta_ij = eta_ij;
        last_synch_data[name_j].tau_j  = tau_j;
        last_synch_data[name_j].tau_i  = tau_i;
      }
      // Send a new delay packet off.
      delay_est.send_del_req(name_j, name, millis());
    }
  }
}

int32_t ATSAlgorithm::virtual_clock(int32_t timestamp) {
    return (int32_t) (alpha_hat * (timestamp - tau_i_ref) + tau_hat_i_ref);
} 