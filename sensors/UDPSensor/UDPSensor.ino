/*
  WiFi UDP Send and Receive String
 
 This sketch wait an UDP packet on localPort using a WiFi shield.
 When a packet is received an Acknowledge packet is sent to the client on port remotePort
 
 Circuit:
 * WiFi shield attached
 
 created 30 December 2012
 by dlf (Metodo2 srl)

 */
#include <WiFi.h>
#include <WiFiUdp.h>
#include <SPI.h>
#include <TimerThree.h>
#include <TimerFour.h>
#include <stdint.h> 

#include "message_formats.h"
#include "netcomms.h"
#include "clock_algorithms.h"

#define HALF_SEC_uS 500000L
#define TEN_SEC_uS 10000000L


#define LED_PIN 8


// --- Start Timer3
volatile bool timer3_overflow = false;
void timer3_set() {
  timer3_overflow = true; 
}

bool timer3_overflowed() {
  if (timer3_overflow) {
    timer3_overflow = false;
    return true;
  } else { 
    return false;
  }
}

bool synch_interval_expired() {
  if (timer3_overflow) {
    timer3_overflow = false;
    return true;
  } else { 
    return false;
  }
}

// Global declaration of the clock algorithm. Use Preprocessor to go between.

ClockAlgorithm clk_alg;

void setup() {
  // FOR LED
  pinMode(LED_PIN, OUTPUT);

  // ----------
  setup_wifi();

  RegistrationProtocol reg_protocol;
  reg_protocol.initialize();
  
  Timer3.initialize(TEN_SEC_uS);
  Timer3.attachInterrupt(timer3_set);
  Timer3.start();

  int16_t registered = false;
  while (!registered) {          
    g_udp_manager.grab_udp_packets();
    for (uint8_t i = 0; i < UDP_BUFFER_SIZE && g_udp_manager.udpbuffer[i].valid; i++) {
      if (reg_protocol.reg_ackn_arrived(&g_udp_manager.udpbuffer[i])) {
        registered = true;
        Timer3.stop();
        break;
      }
    }
    if (timer3_overflowed()) {
      reg_protocol.send_reg_init();
      Serial.println("Sending Registration Initialisation Packet");
    }
  }
  
  // Now wait for FIRST neighbours packet.
  int16_t neighbours_known = false;
  while (!neighbours_known) {          
    g_udp_manager.grab_udp_packets();
    for (uint8_t i = 0; i < UDP_BUFFER_SIZE && g_udp_manager.udpbuffer[i].valid; i++) {
      if (reg_protocol.reg_neighlst_arrived(&g_udp_manager.udpbuffer[i])) {
        Serial.println("Received Reg Neighlst");
        neighbours_known = true;
        break;
      }
    }
  }

  Serial.println("\nRegistration Complete!");
  clk_alg.initialize(&reg_protocol.ackn_packet);
  
  clk_alg.update_neighbours(reg_protocol.neig_packet.neighbours);
}
// int32_t start_routine = 0;
// int32_t end_loop = 0;
bool update_led_hi = false;
bool update_led_lo = true;
uint8_t led_state = 0x00;
#define LED_PERIOD 10000
#define LED_DUTY   0.60
void loop() {  

  clk_alg.routine_work(); // Give ClkAlg the opportunity to do routine_work.  
  g_udp_manager.grab_udp_packets();
  for (uint8_t i = 0; i < UDP_BUFFER_SIZE && g_udp_manager.udpbuffer[i].valid; i++) {
    g_udp_manager.udpbuffer[i].valid = false;
    clk_alg.recv_packet(&g_udp_manager.udpbuffer[i]);
  }

  // Led Flash work!
  int32_t timestamp = millis();
  int32_t curr_virtual = clk_alg.virtual_clock(timestamp);
  if (((curr_virtual % LED_PERIOD) > LED_DUTY*LED_PERIOD) && update_led_hi) {
    update_led_hi = false;
    update_led_lo = true;
    digitalWrite(LED_PIN, 1);
  }   
  else if (((curr_virtual % LED_PERIOD) <= LED_DUTY*LED_PERIOD) && update_led_lo) {
    update_led_lo = false;
    update_led_hi = true;
    digitalWrite(LED_PIN, 0);
  }

  // Do work that doesn't need datagrams and stuff. Make sure you don't spend
  // too much time in here.  
}
