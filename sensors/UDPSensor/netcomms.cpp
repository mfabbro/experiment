#include <Wifi.h>
#include <stdint.h> 
#include <WiFiUdp.h>
#include <SPI.h>
#include "netcomms.h"

/* --- Variable Definitions --- */
char wifi_ssid[] = "lambonbrunswick";   // WiFi Network SSID.
char wifi_pass[] = "ilovelamb";         // WiFi Network Password.

UDPManager g_udp_manager;                       // Global access to Udp port that represents network.

//char g_recvd_dgram_buffer[MAX_DGRAM_LEN]; // Buffer to hold incoming packet

WiFiUDP       UDPManager::udp_sock;
IPAddress     UDPManager::server_ip(10, 0, 1, 2);
uint16_t      UDPManager::server_port    = 4000;
uint16_t      UDPManager::udplisten_port = 3000;
udpdata       UDPManager::udpbuffer[UDP_BUFFER_SIZE];

/* --- Function Definitions --- */
void UDPManager::initialize_udp() {
    udp_sock.begin(udplisten_port);    
}

#define FULL_TX_BUFFER 600
#define MAX_SINGLE_UDP_PACKET 90

void UDPManager::send_packet(char packet[], int16_t len) {
  udp_sock.beginPacket(server_ip, server_port);
  udp_sock.write((uint8_t *) packet, len);
  udp_sock.endPacket();
}
void UDPManager::grab_udp_packets() {
  int32_t timestamp = 0;
  int16_t readlen   = 0;
  int32_t i         = 0;
  int8_t  readcount = 0;
  for (uint8_t i=0; (i < UDP_BUFFER_SIZE) && udp_sock.parsePacket(); i++) {
    readcount++;
    timestamp = millis();
    readlen = udp_sock.read(udpbuffer[i].mesg, MAX_DGRAM_LEN);
    if(readlen > 0 && readlen < MAX_DGRAM_LEN) {
      udpbuffer[i].timestamp = timestamp;
      udpbuffer[i].length    = readlen;
      udpbuffer[i].valid     = true;
    } else {
      udpbuffer[i].valid     = false;      
    }
  }
  if (readcount > 0) {
    Serial.print(readcount); Serial.println(":  packets arrived");    
  }

}

// void UDPManager::grab_packet() {
//   int16_t readlen = udp_sock.read(datagram, MAX_DGRAM_LEN);
//   if (readlen > 0 && readlen < MAX_DGRAM_LEN) {
//     datagram[readlen] = 0;
//     datagram_length = readlen;
//   } else {
//     datagram_length = 0;
//   }
// }

void setup_wifi() {
  int16_t status = WL_IDLE_STATUS;

  //Initialize serial and wait for port to open:
  Serial.begin(SERIAL_BAUD_RATE); 
  while (!Serial)
    ; // wait for serial port to connect. Needed for Leonardo only

  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present"); 
    // don't continue:
    while(true);
  } 

  // attempt to connect to the WiFi network:
  while (status != WL_CONNECTED) { 
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(wifi_ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:    
    status = WiFi.begin(wifi_ssid, wifi_pass);
  } 
  Serial.println("Connected to wifi");
  printWifiStatus();

  Serial.println("\nStarting connection to server...");

  // if you get a connection, report back via serial:
  g_udp_manager.initialize_udp();    
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  int32_t rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

